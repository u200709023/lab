public class FindMin {
    public static void main(String[] args){
        //Find Min
        int n1 = Integer.parseInt(args[0]);
        int n2 = Integer.parseInt(args[1]);
        int n3 = Integer.parseInt(args[2]);
        int result;

        result = n1<n2 ? n1 : n2;
        result = result<n3 ? result : n3;

        System.out.println(result);

        //Find Min2

//        int num1 = Integer.parseInt(args[0]);
//        int num2 = Integer.parseInt(args[1]);
//        int num3 = Integer.parseInt(args[2]);
//
//        if(num1<num2 && num1<num3){
//            System.out.println("Min number : "+ num1);
//        }
//        else if(num2<num3 && num2<num1){
//            System.out.println("Min number : "+ num2);
//        }
//        else{
//            System.out.println("Min number : "+ num3);
//        }


    }
}
